import Api from "./Api";

const base = "/modules/users";


export default {
    async getUsersApi(){
        return await Api().get(base);
    },
    async getUserApi(username){
        return await Api().get(base + '/' + username);
    },
    async saveApi(user){
        return await Api().post(base, user);
    },
    async deleteApi(id){
        return await Api().delete(base + '/' + id)
    },
    async updateApi(user){
        return await Api().put(base, user);
    },
    async uploadCsvApi(file){
        let formData = new FormData();
        formData.append('file', file);
        return await Api().post(base + '/import', formData);
    },
    async getUserRolesInfo(){
        return await Api().get(base + "/userRolesInfo");
    },
    async getGitHubInfo(){
        return await Api().get(base + "/userInfoClient");
    },
    async getReposGithub(){
        return await Api().get(base + '/repos');
    },
    async GetBranchesGithub(name){
        return await Api().get(base + '/repos/branches', {params: {name: name}});
    },
    async getRoles(){
        return await Api().get(base + '/roles');
    },
    async getManagers(){
        return await Api().get(base + '/managers');
    }

}

