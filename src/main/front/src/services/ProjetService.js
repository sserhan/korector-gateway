import Api from "./Api";

const base = "/projets";

export default {
    async getAllByGroupe(id) {
        return await Api().get(base + '/group/' + id);
    },
    async getAllByUser(id){
        return await Api().get(base + '/user/' + id);
    },
    async update(projet){
        return await Api().put(base, projet);
    },
    async create(projet){
        return await Api().post(base, projet);
    },
    async delete(id){
        return await Api().delete(base + '/' + id);
    },
    async getOne(id){
        return await Api().get(base + '/' + id);
    },
}