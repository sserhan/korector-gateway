import axios from 'axios'

export default() => {
    return axios.create({
        baseURL: `/users/api/v1`,
    })
}
