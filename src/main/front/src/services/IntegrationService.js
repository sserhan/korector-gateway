import Api from "./Api";

const base = "/integration";

export default {
    async getAllBuildTools(){
        return Api().get(base + '/buildTools');
    },
    async getBuildTools(id){
        return Api().get(base + '/buildTools/' + id);
    },
    async getAnalyseByProjet(id){
        return Api().get(base + '/analyses/projet/' + id);
    },
    async getAnalyseStatus(id){
        return Api().get(base + '/analyses/status/' + id);
    },
    async rebuildAnalyse(id){
        return Api().get(base + '/analyses/rebuild/' + id);
    }
}