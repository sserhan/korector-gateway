import GatewayApi from "./GatewayApi";

export default {
    async getUserInfoApi(){
        return await GatewayApi().get('/userInfo');
    },
    async getUserGithubLink(){
        return await GatewayApi().get('/github/account-linking');
    }
}

