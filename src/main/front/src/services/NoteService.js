import Api from "./Api";

const base = "/notes";

export default {
    async getAllNotes() {
        return await Api().get(base);
    },

    async getNotesByAnalyse(id){
        return await Api().get(base + '/analyse/' + id);
    },

    async getOneNote(id){
        return await Api().get(base + '/' + id);
    },

    async genererNote(note) {
        return await Api().post(base + "/noteTotal",note);
    },

    async saveNote(note){
        return await Api().post(base, note);
    },

    async updateNote(note){
        return await Api().put(base,note);
    },

    async deleteNote(id){
        return await Api().delete(base ,{params: {id: id}});
    },
}
