import Api from "./Api";

const base = "/metrics"
export default {
    async getAllMetrics() {
        return await Api().get(base);
    },

    async getAllFrom(provenance) {
        return await Api().get(base + '/' + provenance);
    },

    async getAllFromAnalyse(id) {
        return await Api().get(base + '/analyse/' + id);
    },

    async getOneMetric(id) {
        return await Api().get(base + '/' + id);
    },

    async saveMetric(metric) {
        return await Api().post(base, metric);
    },

    async updateMetric(metric) {
        return await Api().put(base, metric);
    },

    async deleteMetric(id) {
        return await Api().delete(base + '/' + id);
    },
}
