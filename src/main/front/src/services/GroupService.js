import Api from "./Api";

const base = "/groupes";

export default {
    async getAll() {
       return await Api().get(base);
    },
    async update(group){
        return await Api().put(base, group);
    },
    async create(group){
        return await Api().post(base, group);
    },
    async delete(id){
        return await Api().delete(base, {params: {id: id}});
    },
    async getOne(id){
        return await Api().get(base + '/' + id);
    }
}