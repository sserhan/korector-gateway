import Api from "./Api";

const base = "modules/analyses";

export default {
    async saveApi(analyse){
        return await Api().post(base, analyse);
    },
}