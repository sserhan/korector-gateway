import Vue from 'vue'
import VueRouter from 'vue-router'
import Users from "../views/Users";
import Profile from "../views/Profile";
import Groups from "../views/Groups";
import Analyse from "../views/Analyse";
import GroupDetail from "../components/groups/GroupDetail";
import Projets from "../views/Projets";


Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/',
            alias: '/groups',
            component: Groups
        },
        {
            path: '/users',
            component: Users
        },
        {
            path: '/profile',
            component: Profile
        },
        {
            path: '/analyses',
            component: Analyse
        },
        {
            path: '/groups/:id',
            name: 'groupDetail',
            component: GroupDetail,
            props: castRouteParams
        },
        {
            path: '/projets',
            component: Projets
        }
    ]
})

function castRouteParams(route) {
    return {
        id: Number(route.params.id),
    };
}