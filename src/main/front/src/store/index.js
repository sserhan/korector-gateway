import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user.store'
import profile from './modules/profile.store'
import group from './modules/group.store'
import analyse from './modules/analyse.store'
import projet from './modules/projet.store'
import metric from './modules/metric.store.js'
import note from './modules/note.store'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    profile,
    group,
    analyse,
    projet,
    metric,
    note,
  }
})
