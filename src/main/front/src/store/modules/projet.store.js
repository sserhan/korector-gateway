import ProjetService from "../../services/ProjetService";

const state= {
    projet: {
        id: null,
        nomProjet: '',
        nomGroupe: '',
        gitRepoName: '',
        gitProvider: '',
        analyses: []
    },
    newProjet: {
        id: '',
        nomProjet: '',
        groupeId: null,
        gitRepoName: '',
        gitProvider:''
    },
    newAnalyse: {
        id: '',
        name: '',
        repository: '',
        build: {
            name: '',
            params: [],
            command:''
        },
        branche: '',
    },
    projets: [],
    errors: []
};
const getters={
    projet: state => state.projet,
    newProjet: state => state.newProjet,
    projets: state => state.projets,
    errors: state => state.errors
};
const mutations = {
    SET_PROJETS(state, projets){
        state.projets = projets;
    },
    SET_PROJET(state, projet){
        state.projet = projet;
    },
    ADD_ANALYSE(state, analyse){
        state.projet.analyses.push(analyse);
    },
    SET_ERRORS(state, errors) {
        state.errors = errors
    },
    ADD_ERROR(state, error) {
        state.errors.push(error);
    },
    ADD_PROJET(state, projet) {
        state.projets.push(projet);
    },
    CLEAR_FORM(state) {
        state.newAnalyse = {
            id: '',
            nomProjet: '',
            groupeId: null,
            gitRepoName: '',
            gitProvider:''
        };
        state.newProjet = {
            id: '',
            nomProjet: '',
            groupeId: null,
            gitRepoName: '',
            gitProvider:''
        };
    },
    CLEAR_STATE(state) {
        state.projet = '';
        state.projets = [];
    },
    CLEAR_ERRORS(state) {
        state.errors = [];
    },
};
const actions = {
    async allByUser({commit}, id){
        await ProjetService.getAllByUser(id)
            .then(response => {
                commit('SET_PROJETS', response.data)
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);

            });
    },
    async allByGroupe({commit}, id){
        await ProjetService.getAllByGroupe(id)
            .then(response => {
                commit('SET_PROJETS', response.data);
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);

            });
    },
    async getProjet({commit}, id){
      await ProjetService.getOne(id)
          .then(response => commit('SET_PROJET', response.data))
          .catch(error => {
              console.log(error);
              commit('ADD_ERROR');
          })
    },
    async save({commit}){
        commit('CLEAR_ERRORS');
        if(!state.newProjet) return;
        await ProjetService.create(state.newProjet)
            .then(response => {
                commit('ADD_PROJET', response.data);
                commit('CLEAR_FORM');
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);
            });
    },
    clearForm({commit}){
        commit('CLEAR_FORM');
        commit('CLEAR_ERRORS');
    },
};
export default {
    namespaced: true, state, getters, mutations, actions,
}