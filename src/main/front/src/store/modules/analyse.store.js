import AnalyseService from "../../services/AnalyseService";
import IntegrationService from "../../services/IntegrationService";

const state= {
    analyse: '',
    newAnalyse: {
        id: '',
        name: '',
        repository: '',
        build: {
            name: '',
            params: [],
            command:''
        },
        branche: '',
    },
    analyses: [],
    errors: []
};
const getters={
    analyse: state => state.analyse,
    newAnalyse: state => state.newAnalyse,
    analyses: state => state.analyses,
    errors: state => state.errors
};
const mutations = {
    SET_ANALYSES(state, analyses){
        state.analyses = analyses;
    },
    SET_ANALYSE(state, analyse){
        state.analyse = analyse;
    },
    SET_ERRORS(state, errors) {
        state.errors = errors
    },
    ADD_ERROR(state, error) {
        state.errors.push(error)
    },
    ADD_ANALYSE(state, analyse) {
        state.analyses.push(analyse);
    },
    UPDATE_RESULTAT(state, analyse) {
        const item = state.analyses.find(item => item.id === analyse.id);
        Object.assign(item, analyse);
    },
    CLEAR_FORM(state) {
        state.newAnalyse = {
            id: '',
            name: '',
            repository: '',
            build: {
                name: '',
                params: [],
                command:''
            },
            branche: '',
        }
    },
    CLEAR_STATE(state) {
        state.analyse = '';
    },
    CLEAR_ERRORS(state) {
        state.errors = [];
    },
};
const actions = {
    async save({commit}) {
        commit('CLEAR_ERRORS');
        if(!state.newAnalyse) return;
        await AnalyseService.saveApi(state.newAnalyse)
            .then(response => {
                commit('ADD_ANALYSE', response.data);
                commit('CLEAR_FORM')
            })
            .catch(error => {
                console.error(error);
                commit('ADD_ERROR',error);
            })
    },
    async allByProjet({commit}, id){
        await IntegrationService.getAnalyseByProjet(id)
            .then(response => commit('SET_ANALYSES', response.data))
            .catch(error => commit('ADD_ERROR', error))
    },
    async updateStatut({commit}, analyse){
        await IntegrationService.getAnalyseStatus(analyse.id)
            .then(response => {
                analyse.resultat = response.data;
                commit('UPDATE_RESULTAT', analyse);
            })
            .catch(error => {
                console.error(error);
                commit('ADD_ERROR',error);
            })
    },
    clearForm({commit}){
        commit('CLEAR_FORM');
    },
};
export default {
    namespaced: true, state, getters, mutations, actions,
}