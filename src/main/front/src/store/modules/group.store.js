import GroupService from '../../services/GroupService';
import UserService from "../../services/UserService";

const state = {
    groups: [],
    newGroup: {
        id: '',
        nom: '',
        description: '',
        users: [],
        managers: [],
    },
    group: {
        id: '',
        nom:'',
        description:'',
        users: [],
        projets: [],
        managers: [],
    },
    errors: []
};

const getters = {
    groups: state => state.groups,
    newGroup: state => state.newGroup,
    errors: state => state.errors,
    getGroupById: state => id => {
        return state.groups.find(group => group.id === id);
    },
    group: state => state.group
};

const actions = {
    async all({commit}){
        commit('CLEAR_ERRORS');
        await GroupService.getAll()
            .then(response => {
                commit('SET_GROUPS', response.data);
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);
            });
    },
    async update({commit}){
        commit('CLEAR_ERRORS');
        if(!state.newGroup) return;
        let usersId = [];
        let managersId = [];
        state.newGroup.users.forEach(user => usersId.push(user.id));
        state.newGroup.managers.forEach(user => managersId.push(user.id));
        let form = {
            id: state.newGroup.id,
            nom: state.newGroup.nom,
            description: state.newGroup.description,
            users: usersId,
            managers: managersId,
        }
        await GroupService.update(form)
            .then(response =>{
                commit('UPDATE_GROUP', response.data);
                commit('CLEAR_FORM')
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);
            })
    },
    async create({commit}) {
        commit('CLEAR_ERRORS');
        if (!state.newGroup) return;
        let usersId = [];
        let managersId = [];
        state.newGroup.users.forEach(user => usersId.push(user.id));
        state.newGroup.managers.forEach(user => managersId.push(user.id));
        let form = {
            id: state.newGroup.id,
            nom: state.newGroup.nom,
            description: state.newGroup.description,
            users: usersId,
            managers: managersId,
        }
        await GroupService.create(form)
            .then(response => {
                commit('ADD_GROUP', response.data);
                commit('CLEAR_FORM');
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);
            })
    },
    async delete({commit}, group){
        commit('CLEAR_ERRORS');
        await GroupService.delete(group.id)
            .then(() => {
                commit('DELETE_GROUP', group);
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);
            });
    },
    async get({commit}, id){
        await GroupService.getOne(id)
            .then(response => {
                commit('SET_GROUP', response.data)
                let users = [];
                response.data.users.forEach(user => {
                    UserService.getUserApi(user)
                        .then(response => users.push(response.data))
                        .catch(error => {
                            console.error(error);
                            commit('SET_ERRORS', error.response.data);
                        })
                });
                commit('SET_USERS_GROUP', users);
                let managers = [];
                response.data.managers.forEach(user => {
                    UserService.getUserApi(user)
                        .then(response => managers.push(response.data))
                        .catch(error => {
                            console.error(error);
                            commit('ADD_ERROR', error);
                        })
                });
                commit('SET_MANAGERS_GROUP', managers);
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);
            });
    },
    clearForm({commit}){
        commit('CLEAR_FORM');
        commit('CLEAR_ERRORS');
    },
    removeUserGroupForm({commit}, user){
      commit('REMOVE_USER_GROUP_FORM', user);
    },
    removeManagerGroupForm({commit}, user){
        commit('REMOVE_MANAGER_GROUP_FORM', user);
    }
};

const mutations = {
    SET_GROUPS(state, groups) {
        state.groups = groups;
    },
    SET_GROUP(state, group){
        state.group = group
    },
    SET_USERS_GROUP(state, users){
        state.group.users = users;
    },
    SET_MANAGERS_GROUP(state, managers){
        state.group.managers = managers;
    },
    CLEAR_ERRORS(state) {
        state.errors = [];
    },
    SET_ERRORS(state, errors) {
        state.errors = errors
    },
    ADD_ERROR(state, error) {
        state.errors.push(error);
    },
    CLEAR_FORM(state) {
        state.newGroup = {
            id:'',
            name: '',
            desc: '',
            users: [],
            managers: []
        }
    },
    ADD_GROUP(state, group) {
        state.groups.push(group);
    },
    UPDATE_GROUP(state,group){
        const item = state.groups.find(item => item.id === group.id);
        Object.assign(item,group);
    },
    DELETE_GROUP(state,group){
        state.groups = state.groups.filter(item => item.id !== group.id);
    },
    REMOVE_USER_GROUP_FORM(state, user){
        state.newGroup.users = state.newGroup.users.filter(item => item.id !== user.id);
    },
    REMOVE_MANAGER_GROUP_FORM(state, manager){
        state.newGroup.managers = state.newGroup.managers.filter(item => item.id !== manager.id);
    }
};

export default {
    namespaced: true, state, getters, actions, mutations
}