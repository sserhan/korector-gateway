import MetricService from "../../services/MetricService";

const state = {
    metrics: [],
    newMetric: {
        id: '',
        nom: '',
        alias: '',
        unite: '',
        provenance: '',
        poids: '',
        valeur: '',
        valeurComparaison: '',
    },
    metric: '',
    errors: [],
};

const getters={
    metric: state => state.metric,
    newMetric: state => state.newMetric,
    metrics: state => state.metrics,
    errors: state => state.errors,
};

const mutations = {
    SET_METRICS(state, metrics){
        state.metrics = metrics;
    },
    SET_METRIC(state, metric){
        state.metric = metric;
    },
    CLEAR_ERRORS(state) {
        state.errors = [];
    },
    SET_ERRORS(state, errors) {
        state.errors = errors
    },
    ADD_ERROR(state, error) {
        state.errors.push(error);
    },
    CLEAR_STATE(state){
        state.metric = '';
        state.metrics = [];
    },
    CLEAR_FORM(state){
        state.newMetric = {
            id: '',
            nom: '',
            alias: '',
            unite: '',
            provenance: '',
            poids: '',
            valeur: '',
            valeurComparaison: '',
        }
    },
    ADD_METRIC(sate, metric){
        state.metrics.push(metric);
    },
    UPDATE_METRIC(state,metric){
        const item = state.metrics.find(item => item.id === metric.id);
        Object.assign(item,metric);
    },
    DELETE_METRIC(state,metric){
        state.metrics = state.metrics.filter(item => item.id !== metric.id);
    },
};

const actions = {
    async all({commit}){
        commit('CLEAR_ERRORS');
        await MetricService.getAllMetrics()
            .then(response => {
                commit('SET_METRICS', response.data)
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);
            });
    },
    async allFrom({commit}, provenance){
        commit('CLEAR_ERRORS');
        await MetricService.getAllFrom(provenance)
            .then(response => {
                commit('SET_METRICS', response.data)
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);
            });
    },
    async allFromAnalyse({commit}, id){
        commit('CLEAR_ERRORS');
        await MetricService.getAllFromAnalyse(id)
            .then(response => {
                commit('SET_METRICS', response.data)
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);
            });
    },
    async get({commit}, id){
        commit('CLEAR_ERRORS');
        await MetricService.getOneMetric(id)
            .then(response => {
                commit('SET_METRIC', response.data)
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);
            });
    },
    async save({commit}){
        commit('CLEAR_ERRORS');
        if(!state.newMetric)return;
        await MetricService.saveMetric(state.newMetric)
            .then(response => {
                commit('ADD_METRIC', response.data);
                commit('CLEAR_FORM');
            })
            .catch(error => {
                console.error(error);
                commit('ADD_ERROR', error);
            })
    },
    async update({commit}){
        commit('CLEAR_ERRORS');
        if(!state.newMetric)return;
        await MetricService.updateMetric(state.newMetric)
            .then(response => {
                commit('UPDATE_METRIC', response.data);
                commit('CLEAR_FORM');
            })
            .catch(error => {
                console.error(error);
                commit('ADD_ERROR', error);
            })
    },
    async delete ({commit}, metric){
        commit('CLEAR_ERRORS');
        await MetricService.deleteMetric(metric.id)
            .then(() => {
                commit('DELETE_METRIC', metric);
            })
            .catch(error => {
                console.error(error);
                commit('ADD_ERROR', error.response.data);
            })
    },
    clearForm({commit}){
        commit('CLEAR_FORM');
        commit('CLEAR_ERRORS');
    },
};

export default {
    namespaced: true, state, getters, actions, mutations
}