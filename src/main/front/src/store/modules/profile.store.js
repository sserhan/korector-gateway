import ProfileService from "../../services/ProfileService";
import UserService from "../../services/UserService";

const state = {
    profile: {
        id:'',
        username:'',
        lastName:'',
        firstName:'',
        email: '',
        github: false,
        githubUsername:'',
        avatarUrl:'',
        roles: []
    }
};
const getters = {
    profile: state => state.profile
};
const mutations = {
    SET_PROFILE(state, profile) {
        state.profile = {
            id: profile.id,
            username: profile.username,
            lastName: profile.lastName,
            firstName: profile.firstName,
            email: profile.email,
            roles: [],
            github: false,
            githubUsername: '',
            avatarUrl: '',
        };
    },
    SET_ROLES(state, profile) {
        state.profile = {
            id: state.profile.id,
            username: state.profile.username,
            lastName: state.profile.lastName,
            firstName: state.profile.firstName,
            email: state.profile.email,
            roles: profile.roles,
            github: false,
            githubUsername: '',
            avatarUrl: '',
        };
    },
    SET_GITHUB(state, github){
        state.profile = {
            id: state.profile.id,
            username: state.profile.username,
            lastName: state.profile.lastName,
            firstName: state.profile.firstName,
            email: state.profile.email,
            roles: state.profile.roles,
            github: github.github,
            githubUsername: github.username,
            avatarUrl: github.avatarUrl,
        };
    }
};
// TODO refaire fonction auhenticate au propre
const actions = {
    async loadProfile({commit}){
      await ProfileService.getUserInfoApi()
          .then(response => {
              commit('SET_PROFILE', response.data);
              UserService.getUserRolesInfo()
              .then(response => {
               commit('SET_ROLES', response.data);   
                UserService.getGitHubInfo().then(response => {
                commit('SET_GITHUB', response.data);
              })
              });
          })
    },
};
export default {
    namespaced: true, state, getters, mutations, actions,
};