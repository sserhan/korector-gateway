import NoteService from "../../services/NoteService";

const state = {
    notesAnalyse: [
        {
            analyseId: '',
            notes: [],
        }
    ],
    notes: [],
    newNote: {
        id: '',
        metrics: [],
    },
    note: '',
    errors: [],
};

const getters={
    notesAnalyse: (state) => (analyseId) => {
        return state.notesAnalyse[analyseId];
    },
    note: state => state.note,
    newNote: state => state.newNote,
    notes: state => state.notes,
    errors: state => state.errors,
};

const mutations = {
    SET_NOTES_ANALYSE(state, notes, analyseId){
        state.notesAnalyse[analyseId] = notes;
    },
    SET_NOTES(state, notes){
        state.notes = notes;
    },
    SET_NOTE(state, note){
        state.note = note;
    },
    CLEAR_NOTES(state){
        state.notes = [];
    },
    CLEAR_ERRORS(state) {
        state.errors = [];
    },
    SET_ERRORS(state, errors) {
        state.errors = errors
    },
    ADD_ERROR(state, error) {
        state.errors.push(error);
    },
    CLEAR_STATE(state){
        state.note = '';
        state.notes = [];
    },
    CLEAR_FORM(state){
        state.newNote = {
            id: '',
            metrics: [],
        }
    },
    ADD_NOTE(sate, note){
        state.notes.push(note);
    },
    UPDATE_NOTE(state,note){
        const item = state.notes.find(item => item.id === note.id);
        Object.assign(item,note);
    },
    DELETE_NOTE(state,note){
        state.notes = state.notes.filter(item => item.id !== note.id);
    },
};

const actions = {
    async all({commit}){
        commit('CLEAR_ERRORS');
        await NoteService.getAllNotes()
            .then(response => {
                commit('SET_NOTES', response.data)
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);
            });
    },
    async allByAnalyse({commit}, id){
        commit('CLEAR_ERRORS');
        await NoteService.getNotesByAnalyse(id)
            .then(response => {
                console.log("SET note analyses with " + response.data + " AND ID = " + id);
                commit('SET_NOTES', response.data);
            })
            .catch(error => commit('ADD_ERROR', error));
    },
    async get({commit}, id){
        commit('CLEAR_ERRORS');
        await NoteService.getOneNote(id)
            .then(response => {
                commit('SET_NOTE', response.data)
            })
            .catch(error => {
                console.error(error);
                commit('SET_ERRORS', error.response.data);
            });
    },
    async genererNote({commit}){
        commit('CLEAR_ERRORS');
        if(!state.newNote)return;
        await NoteService.genererNote(state.newNote)
            .then(response => {
                commit('SET_NOTE', response.data);
                commit('CLEAR_FORM');
            })
            .catch(error => {
                console.error(error);
                commit('ADD_ERROR', error);
            })
    },
    async save({commit}){
        commit('CLEAR_ERRORS');
        if(!state.newNote)return;
        await NoteService.saveNote(state.newNote)
            .then(response => {
                commit('ADD_NOTE', response.data);
                commit('SET_NOTE', response.data);
                commit('CLEAR_FORM');
            })
            .catch(error => {
                console.error(error);
                commit('ADD_ERROR', error);
            })
    },
    async update({commit}){
        commit('CLEAR_ERRORS');
        if(!state.newMetric)return;
        await NoteService.updateNote(state.newNote)
            .then(response => {
                commit('UPDATE_NOTE', response.data);
                commit('CLEAR_FORM');
            })
            .catch(error => {
                console.error(error);
                commit('ADD_ERROR', error);
            })
    },
    async delete ({commit}, note){
        commit('CLEAR_ERRORS');
        await NoteService.deleteNote(note.id)
            .then(() => {
                commit('DELETE_NOTE', note);
            })
            .catch(error => {
                console.error(error);
                commit('ADD_ERROR', error.response.data);
            })
    },
    clearForm({commit}){
        commit('CLEAR_FORM');
    },
};

export default {
    namespaced: true, state, getters, actions, mutations
}