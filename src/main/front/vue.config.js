module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  devServer: {
    proxy: {
      '/api': {
        target: 'http://13.69.75.86:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },
      '/client': {
        target: 'http://13.69.75.86:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },
      '/users': {
        target: 'http://13.69.75.86:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },
      '/analyse': {
        target: 'http://13.69.75.86:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true,
      },
      '/logout': {
        target: 'http://13.69.75.86:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },
      '/private': {
        target: 'http://13.69.75.86:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },
      '/oauth2/authorization/login-client': {
        target: 'http://13.69.75.86:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },
      '/login/oauth2/code/login-client': {
        target: 'http://13.69.75.86:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },

    }
  },
};