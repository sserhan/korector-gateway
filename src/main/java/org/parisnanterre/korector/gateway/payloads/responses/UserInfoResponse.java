package org.parisnanterre.korector.gateway.payloads.responses;

import java.util.HashSet;
import java.util.Set;

public class UserInfoResponse {

    private String id;

    private String firstName;

    private String username;

    private String lastName;

    private String email;

    private boolean github;

    private String githubUsername;

    private String githubAvatar;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isGithub() {
        return github;
    }

    public void setGithub(boolean github) {
        this.github = github;
    }

    public String getGithubUsername() {
        return githubUsername;
    }

    public void setGithubUsername(String githubUsername) {
        this.githubUsername = githubUsername;
    }

    public String getGithubAvatar() {
        return githubAvatar;
    }


    public void setGithubAvatar(String githubAvatar) {
        this.githubAvatar = githubAvatar;
    }

}
