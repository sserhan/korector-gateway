package org.parisnanterre.korector.gateway.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.keycloak.common.util.Base64Url;
import org.keycloak.common.util.KeycloakUriBuilder;
import org.parisnanterre.korector.gateway.config.GithubConfig;
import org.parisnanterre.korector.gateway.config.KeycloakConfig;
import org.parisnanterre.korector.gateway.payloads.responses.UserInfoResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
@RestController
@RequestMapping("/api/v1")
public class UserInfoControllerV1 {

    @GetMapping("/userInfo")
    public ResponseEntity<UserInfoResponse> getUser(@AuthenticationPrincipal OidcUser oidcUser)  {
        System.out.println("OIDC USER");
        oidcUser.getClaims().forEach((k,v) -> System.out.println("key: " + k + " , value: " + v));
        System.out.println("CLAIMS");
        oidcUser.getUserInfo().getClaims().forEach((k,v) -> System.out.println("key: " + k + " , value: " + v));
        System.out.println("ATTRIBUTES");
        oidcUser.getAttributes().forEach((k,v) -> System.out.println("key: " + k + " , value: " + v));
        System.out.println("AUDIANCE");
        oidcUser.getAudience().forEach(System.out::println);
        System.out.println("AUTHORITIES");
        oidcUser.getAuthorities().forEach(x -> System.out.println(x.toString()));
        UserInfoResponse userInfoResponse = new UserInfoResponse();
        userInfoResponse.setId(oidcUser.getClaimAsString("sub"));
        userInfoResponse.setEmail(oidcUser.getEmail());
        userInfoResponse.setFirstName(oidcUser.getGivenName());
        userInfoResponse.setLastName(oidcUser.getFamilyName());
        userInfoResponse.setUsername(oidcUser.getPreferredUsername());

        return new ResponseEntity<>(userInfoResponse, HttpStatus.OK);
    }


}
