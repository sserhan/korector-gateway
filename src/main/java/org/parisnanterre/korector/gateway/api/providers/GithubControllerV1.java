package org.parisnanterre.korector.gateway.api.providers;

import org.keycloak.common.util.Base64Url;
import org.keycloak.common.util.KeycloakUriBuilder;
import org.parisnanterre.korector.gateway.config.GithubConfig;
import org.parisnanterre.korector.gateway.config.KeycloakConfig;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/github")
public class GithubControllerV1 {

    private final KeycloakConfig config;

    private final GithubConfig githubConfig;

    public GithubControllerV1(KeycloakConfig config, GithubConfig githubConfig) {
        this.config = config;
        this.githubConfig = githubConfig;
    }


    @GetMapping("/account-linking")
    public String synchronizeGithub(@AuthenticationPrincipal OidcUser oidcUser) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        /* Création d'un lien de redirection de keycloak à github source : https://github.com/keycloak/keycloak-documentation/blob/master/server_development/topics/identity-brokering/account-linking.adoc
         les infos sont nécessaires sont récupérés via le claim sur le jwt et la config de l'appli */
        String nonce = UUID.randomUUID().toString(); // création du once
        String input = nonce + (String) oidcUser.getClaim("session_state") + (String) oidcUser.getClaim("azp") + "github"; // création de l'input du hash
        byte[] check = md.digest(input.getBytes(StandardCharsets.UTF_8));
        String accountLinkUrl = KeycloakUriBuilder.fromUri(config.getHost())
                .path("/auth/realms/" + config.getRealm() + "/broker/github/link")
                .queryParam("nonce", nonce)
                .queryParam("hash", Base64Url.encode(check)) // création d'un hash
                .queryParam("client_id", (String) oidcUser.getClaim("azp")) // lecture du client id à partir du claim
                .queryParam("redirect_uri", githubConfig.getRedirectUrl()).build(config.getRealm(), "github").toString();
        return accountLinkUrl;
    }
}
