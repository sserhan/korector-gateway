package org.parisnanterre.korector.gateway.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GithubConfig {

    @Value("${github.redirectUrl}")
    private String redirectUrl;

    public String getRedirectUrl() {
        return redirectUrl;
    }
}
